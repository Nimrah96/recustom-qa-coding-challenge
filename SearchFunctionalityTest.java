import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchFunctionalityTest {
    public static void main(String[] args) {
        
        System.setProperty("webdriver.chrome.driver", "path/to/chromedriver.exe");
        
               WebDriver driver = new ChromeDriver();
        
               System.out.println("Test Case 1: Verify Search Functionality");

        driver.get("https://www.haggadot.com/");
        WebElement searchInput = driver.findElement(By.name("q"));

                String keyword = "Passover";
        searchInput.sendKeys(keyword);

             searchInput.sendKeys(Keys.RETURN);

        
        if (driver.findElements(By.className("haggadah")).size() > 0) {
            System.out.println("Pass: Search results loaded successfully.");
        } else {
            System.out.println("Fail: Search results not loaded within timeout.");
        }

               driver.quit();
    }
}
