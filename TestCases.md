# Test Cases for User Story: Search for Haggadah and Share

## Preconditions
- User is logged in to haggadot.com.
- User has access to the search functionality.

## Test-Case 1: To verify that users can search for Haggadahs using a keyword.

### Steps to Produce
1. Navigate to the search bar on haggadot.com.
2. Enter a keyword related to Haggadahs and favorite rituals.
3. Click on the search icon or press Enter.

### Expected Result
A list of Haggadahs matching the keyword is displayed.

## Test-Case 2:To ensure that search results include relevant Haggadahs.

### Steps to Produce 
1. Perform a search for a specific keyword related to Haggadahs.
2. Check that the search results include Haggadahs relevant to the entered keyword.

### Expected Result
The search results contain Haggadahs that match the entered keyword.

## Test-Case 3: To verify that users can share a specific Haggadah from the search results.

### Steps to Produce
1. Perform a search for a keyword related to Haggadahs.
2. Select a specific Haggadah from the search results.
3. Look for the option to share the Haggadah.
4. Click on the share option.

### Expected Result
A sharing modal or dialog appears with options to share the Haggadah.

## Test-Case 4:To ensure that users can copy a shareable link for a specific Haggadah.

### Steps to Produce
1. Perform a search for a keyword related to Haggadahs.
2. Select a specific Haggadah from the search results.
3. Look for the option to copy the shareable link.
4. Click on the option to copy the link.

### Expected Result
A link to the selected Haggadah is copied to the clipboard.

## Test Case 5:To verify that users can share a Haggadah on social media platforms.

### Steps To Produce
1. Perform a search for a keyword related to Haggadahs.
2. Select a specific Haggadah from the search results.
3. Look for options to share the Haggadah on Facebook, Twitter, and LinkedIn.
4. Click on each option to share the Haggadah on the respective social media platforms.

### Expected Result
When sharing on Facebook, Twitter, or LinkedIn, a post containing a link to the selected Haggadah is created.

## Test Case 6: To ensure that the share button is visible for all Haggadahs in the search results.

### Steps To Produce
1. Perform a search for a keyword related to Haggadahs.
2. Check the search results for the presence of a share button next to each Haggadah.

### Expected Result
A share button is visible next to each Haggadah in the search results.
